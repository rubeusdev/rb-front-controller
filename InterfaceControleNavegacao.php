<?php
namespace Rubeus\FrontController;

interface InterfaceControleNavegacao{
    
    public function iniciar($arquivoNavegacao, $urlProjeto);
        
    public function setDependencias($dependencia,$pastaCliente,$arquivoUtilizados);
    
    public function identificarUrl($endereco);
    
    public function getDadosPagina();
    
    public function getDados($idPagina, $op=false);
    
}
