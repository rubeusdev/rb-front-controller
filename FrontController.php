<?php
namespace Rubeus\FrontController;
use Rubeus\Servicos\String\Explode as Explode;
use Rubeus\Servicos\Vetor\Compare as Compare; 
use Rubeus\Servicos\Entrada\I as I;
use Rubeus\Servicos\Json\Json as Json;
use Rubeus\Servicos\Entrada\Sessao as Sessao;

class FrontController{
    private $endereco;
    private $rotas;
    private $explode;
    private $parametro;
    private $compare;
    
    public function __construct() {
        Sessao::iniciar(I::get('codsessrt'));
        $this->endereco = strpos(I::server('REQUEST_URI'), '?') ? 
                substr(I::server('REQUEST_URI'),0, strpos(I::server('REQUEST_URI'), '?')) : I::server('REQUEST_URI');
    }
    
    public function chamarMetodo($classe,$metodo){
        $objeto = new $classe();
        $objeto->$metodo($this->parametro[0], $this->parametro[1], $this->parametro[2]);
    }
    
    public function resolverRota($endereco=false){
        
        if(!$endereco){
            $endereco = $this->endereco;
        }
        $rota = Json::lerArq($this->rotas);
        $this->explode = new Explode();
        $this->compare = new Compare();
        foreach($rota as $key=>$valor){
            if($this->compararRota($key, $endereco, $valor)){                
                $this->chamarMetodo($valor->classe, $valor->metodo);
                return true;
            }
        }        
    } 
    
    public function compararRota($rota, $endereco,$valor){
        $parametrosRota = $this->limparArray($this->explode->explode('/',$rota));
        $parametrosEndereco = $this->limparArray($this->explode->explode('/',$endereco));
        
        $qtd = count($parametrosEndereco);
        $qtdRota = count($parametrosRota);
        
        if($qtd !== $qtdRota) return false;
        for($i = 0; $i < $qtd; $i++){            
            if(strpos($parametrosRota[$i], '}}')===false 
                && $parametrosRota[$i] !== $parametrosEndereco[$i])
                    return false;
                
            if(strpos($parametrosRota[$i], '}}')!==false){
                if(!$this->simularRotaInEnderco($rota, $endereco, $valor->parametros))                    
                    return false;
                
            }
        }
        return true;
    }
    
    public function dividir($array){
        $arrayImpar = array();
        $arrayPares = array();
        $qtd = count($array);
        $entrou = 1;
        for($i = 0; $i < $qtd ;$i++){
            if($entrou == 1){
                $arrayImpar[] = $array[$i];
                $entrou = 0;
            }else{
                $arrayPares[] = $array[$i];
                $entrou = 1;
            }
        }
        return array('fixo' => $arrayPares, 'variavel' => $arrayImpar);
    }
    
    public function simularRotaInEnderco($rota, $endereco, $parametro){
        $rotaArray = $this->limparArray($this->explode->explode('/', $rota));
        $enderecoArray = $this->limparArray($this->explode->explode('/', $endereco));
        for($i =0; $i< count($rotaArray); $i++){
            $chave = $this->limparArray($this->explode->explode(array('{{','}}'),$rotaArray[$i]));
            $array = $this->dividir($chave);
            $valor = $this->limparArray($this->explode->explode($array['fixo'], $enderecoArray[$i]));
            $fixo = $this->limparArray($this->explode->explode( $valor, $enderecoArray[$i]));
           
            if(!$this->compare->igualdade($array['fixo'],$fixo) && !($fixo == false && count($array['fixo'])==0))
                    return false;
            $this->popularParametro($parametro, $chave, $valor);
        }
        return true;
    } 
    
    
    public function popularParametro($parametro, $chave, $valor){
        $qtd = count($parametro);
        $qtdChave = count($chave);
        $valorAtual = '';
        for($i = 0; $i < $qtd; $i++){
            for($j=0; $j<$qtdChave; $j++){
                if($chave[$j] == $parametro[$i]){
                    $valorAtual = $valor[$j];
                    break;
                }
            }
            if (trim($valorAtual) !== '') {
                $this->parametro[$i] = $valorAtual;
                $valorAtual = '';
            } elseif (!isset($this->parametro[$i]) || !$this->parametro[$i]) {
                $this->parametro[$i] = $parametro[$i];
            }
        }
        
    }
    
    public function limparArray($array){
        if(!is_array($array)) return $array;
        $qtd = count($array);
        for($i = 0; $i < $qtd; $i++)
            if($array[$i] === '')unset($array[$i]);
       
        return array_values($array);
    }
    
    public function getEndereco() {
        return $this->endereco;
    }
    
    public function setRota($rota){
        $this->rotas = $rota;
    }    

    public function iniciar(){
        $this->setRota(ROTAS);
        $this->resolverRota();
    }        
}
