<?php
namespace Rubeus\FrontController;
use Rubeus\Servicos\Entrada\Sessao;
use Rubeus\Servicos\Entrada\I;
use Rubeus\Navegacao\ControleNavegacao;
use Rubeus\ContenerDependencia\Conteiner;
use MatthiasMullie\Minify;

class Pagina{

    public function __construct() {
        Sessao::iniciar();
    }

    private function getEndereco(){
        $endereco = strpos(I::server('REQUEST_URI'), '?') ?
                substr(I::server('REQUEST_URI'),0, strpos(I::server('REQUEST_URI'), '?')) : I::server('REQUEST_URI');

         if(!preg_match('/:[0-9]*/', URL_PROJETO,$resultado)){

            $array = explode('/', URL_PROJETO);
            $array[0]='';
            $resultado = implode('/',$array);
        }else{
            $array = explode($resultado[0], URL_PROJETO);
            $resultado = $array[1];
        }
        if($resultado == '/'){
            $resultado = '';
            if(strpos($endereco,'/')==0){
                $endereco = substr($endereco,1,  strlen($endereco));
            }
        }
        return str_replace($resultado, '', $endereco);
    }



    public function identificarUrl($dependencia=false,$idPagina=false){
        $endereco = $this->getEndereco();
        $controleNavegacao = Conteiner::get('ControleNavegacao');
        $controleNavegacao->iniciar(Conteiner::get('ARQ_NAV'), URL_PROJETO);

        $controleNavegacao->setDependencias($dependencia, DIR_BASE.Conteiner::get('PST_CLIENTE'),
                                            Conteiner::get('ARQ_UTILIZADO'));

        if(I::get('idPagina')){
            return $controleNavegacao->getDados(I::get('idPagina'));
        }elseif(!$idPagina){
            $controleNavegacao->identificarUrl($endereco);
            return $controleNavegacao->getDadosPagina();
        }else{
            return $controleNavegacao->getDados($idPagina);
        }
    }

    public function getDados($idPage, $op,$app=false,$retorno=false){
        if($app){
            ConfigurarAmbiente::atualizarConfiguracao($app);
        }
        $controleNavegacao =  Conteiner::get('ControleNavegacao');
        $controleNavegacao->iniciar(Conteiner::get('ARQ_NAV'), PROJETO, URL_PROJETO);

        if(I::get('enderecoChamadaNav')){
            $controleNavegacao->identificarUrl(I::get('enderecoChamadaNav'));
            $dados = $controleNavegacao->getDadosPagina();
        }else{
            $dados = $controleNavegacao->getDados($idPage, $op);
        }

        if($retorno){
            return $dados;
        }
        $dados['dominio']['dominioCliente'] = DOMINIO;
        $dados['dominio']['dominioRequisicao'] = DOMINIO_REQ;
        $dados['dominio']['dominio'] = DOMINIO;

        Saida::ecoar($dados);
    }

    private function limparDiretorio(){
        if(strpos(Conteiner::get('CAMINHO_CSS'), 'ui')){
            foreach (glob(Conteiner::get('CAMINHO_JS').'*') as $filename) {
                unlink($filename);
            }
            foreach (glob(Conteiner::get('CAMINHO_CSS').'*') as $filename) {
                unlink($filename);
            }
        }
    }

    private function pegarIndex(){
        ob_start();
        include Conteiner::get('INDEX');
        $texto = ob_get_contents();
        ob_end_clean();
        return $texto;
    }

    private function subtiturBaseJS($substituir, $novoValor,$js, $dados){
        if (APLICACAO_PRODUCAO == 1) {
            return str_replace($substituir, $novoValor, 'var refAmbienteG = \''.URL_PROJETO.'\';'.
                'var refAmbienteReqG = \''.DOMINIO_REQ.'\';'.
                'var refDominioG = \''.DOMINIO_PROJETO.'\';'.
                (defined('RUBEUS_ACCOUNT_URL') ? 'var refRbAccount = \''. RUBEUS_ACCOUNT_URL.'\';' : 'var refRbAccount = false;').
                $js);
        }
        return str_replace($substituir, $novoValor,'var DGlobal = '.json_encode($dados).'; '.
            'ReqOriginal = [JSON.parse(JSON.stringify(DGlobal))];'.
            'var refAmbienteG = \''.URL_PROJETO.'\';'.
            'var refAmbienteReqG = \''.DOMINIO_REQ.'\';'.
            'var refDominioG = \''.DOMINIO_PROJETO.'\';'.
            (defined('RUBEUS_ACCOUNT_URL') ? 'var refRbAccount = \''. RUBEUS_ACCOUNT_URL.'\';' : 'var refRbAccount = false;').
            $js);
    }


    private function enviarSaida($substituir,$novoValor, $rand,$dados, $texto){
        if(file_exists(DIR_BASE.'/'.VARIAVEIS_AMBIENTE)){
            $dados['variaveisAmbiente'] = json_decode(file_get_contents(DIR_BASE.'/'.VARIAVEIS_AMBIENTE),true);
        }
        if(BASE_URL == 'BASE_URL'){
            define('BASE_URL','/');
        }
        if (APLICACAO_PRODUCAO == 1) {
            $substituir[] = '<!-- DGBLOBAL';
            $substituir[] = 'DGBLOBAL -->';
            $novoValor[] = '';
            $novoValor[] = '';
        }
		echo str_replace($substituir, $novoValor, str_replace(['<base href="/">', '{PHP{BASEURLPROJETO}PHP}','{PHP{Dominio}PHP}','{PHP{ng-app}PHP}', '{PHP{URLPROJETO}PHP} href="', '{PHP{URLPROJETO}PHP} src="', '/*{PHP{URLPROJETO}PHP}*/\'/u', '{PHP{URLPROJETO}PHP}', '{php{urlprojeto}php}="" href="', '{php{urlprojeto}php}="" src="', '{php{urlprojeto}php} href="', '{php{urlprojeto}php} src="', '/*{PHP{URLPROJETO}PHP}*/\'/u', '{PHP{rand}PHP}', '{PHP{DGBLOBAL}PHP}', '{PHP{IDIOMA}PHP}'],
						['<base href="'.BASE_URL.'">', BASE_URL, DOMINIO,Conteiner::get('ng-app'), 'href="'.PROTOCOLLO.'://'.URL_PROJETO, 'src="'.PROTOCOLLO.'://'.URL_PROJETO, '\''.PROTOCOLLO.'://'.URL_PROJETO.'u', PROTOCOLLO.'://'.URL_PROJETO, 'href="'.PROTOCOLLO.'://'.URL_PROJETO, 'src="'.PROTOCOLLO.'://'.URL_PROJETO, 'href="'.PROTOCOLLO.'://'.URL_PROJETO, 'src="'.PROTOCOLLO.'://'.URL_PROJETO, '\''.PROTOCOLLO.'://'.URL_PROJETO.'u', $rand,'var DGlobal = '.json_encode($dados), IDIOMA],
						$texto));
    }

    public function montarJs($dados = false, $substituir = false, $novoValor = false, $identificadoArquivo = false) {
        if (!$dados) {
            $dados = $this->identificarUrl(true, I::get('idPage'));
        }
        if (!$identificadoArquivo) {
            $identificadoArquivo = rand(12, 567567567567);
        }
        $js = $dados['dependencias']['js'];
        unset($dados['dependencias']);

        if (!$substituir || !$novoValor) {
            $substituir = $novoValor = [];
            if(file_exists(DIR_BASE.'/'.SUBSTITUICAO_PASIVA)){
                $substituicaoPassivaText = str_replace(
                    ['{PHP{ng-app}PHP}','{PHP{URLPROJETO}PHP}', '{PHP{IDIOMA}PHP}'],
                    [Conteiner::get('ng-app'),PROTOCOLLO.'://'.URL_PROJETO, IDIOMA],
                    file_get_contents(DIR_BASE.'/'.SUBSTITUICAO_PASIVA));
                $substituicaoPassiva = json_decode($substituicaoPassivaText,true);
                foreach ($substituicaoPassiva as $key=>$value){
                    $substituir[] = $key;
                    $novoValor[] = $value;
                }
            }
        }

        define('IDENTIFICACAO_ARQUIVO', $identificadoArquivo);
        if (defined('FIlES_JS_CSS_MIM') && FIlES_JS_CSS_MIM == 1) {
            $jsMini = new Minify\JS();
            
            $jsMini->add($this->subtiturBaseJS($substituir, $novoValor,$js, $dados));
            file_put_contents(Conteiner::get('CAMINHO_JS').$identificadoArquivo.'.js', $jsMini->minify());
        } else {
            file_put_contents(Conteiner::get('CAMINHO_JS').$identificadoArquivo.'.js', $this->subtiturBaseJS($substituir, $novoValor,$js, $dados));
        }
    }

    public function montarCss($css = false, $substituir = false, $novoValor = false, $identificadoArquivo = false) {
        if (!$css) {
            $dados = $this->identificarUrl(true, I::get('idPage'));
            $css = $dados['dependencias']['css'];
            unset($dados['dependencias']);
        }
        if (!$identificadoArquivo) {
            $identificadoArquivo = rand(12, 567567567567);
        }

        if (!$substituir || !$novoValor) {
            $substituir = $novoValor = [];
            if (file_exists(DIR_BASE.'/'.SUBSTITUICAO_PASIVA)) {
                $substituicaoPassivaText = str_replace(
                    ['{PHP{ng-app}PHP}','{PHP{URLPROJETO}PHP}', '{PHP{IDIOMA}PHP}'],
                    [Conteiner::get('ng-app'),PROTOCOLLO.'://'.URL_PROJETO, IDIOMA],
                    file_get_contents(DIR_BASE.'/'.SUBSTITUICAO_PASIVA));
                $substituicaoPassiva = json_decode($substituicaoPassivaText,true);
                foreach ($substituicaoPassiva as $key=>$value) {
                    $substituir[] = $key;
                    $novoValor[] = $value;
                }
            }
        }

        define('IDENTIFICACAO_ARQUIVO', $identificadoArquivo);
        if (defined('FIlES_JS_CSS_MIM') && FIlES_JS_CSS_MIM == 1) {
            $cssMini = new Minify\CSS();

            $cssMini->add(str_replace($substituir, $novoValor, $css));
            file_put_contents(Conteiner::get('CAMINHO_CSS').$identificadoArquivo.'.css', $cssMini->minify());
        } else {
            file_put_contents(Conteiner::get('CAMINHO_CSS').$identificadoArquivo.'.css', str_replace($substituir, $novoValor, $css));
        }
    }

    private function montarDesenvolvimento(){
        $dados = $this->identificarUrl(true, I::get('idPage'));
        $rand = rand(12, 567567567567);
        $this->limparDiretorio();

        $substituir = $novoValor = [];
        if(file_exists(DIR_BASE.'/'.SUBSTITUICAO_PASIVA)){
            $substituicaoPassivaText = str_replace(
                ['{PHP{ng-app}PHP}','{PHP{URLPROJETO}PHP}', '{PHP{IDIOMA}PHP}'],
                [Conteiner::get('ng-app'),PROTOCOLLO.'://'.URL_PROJETO, IDIOMA],
                file_get_contents(DIR_BASE.'/'.SUBSTITUICAO_PASIVA));
            $substituicaoPassiva = json_decode($substituicaoPassivaText,true);
            foreach ($substituicaoPassiva as $key=>$value){
                $substituir[] = $key;
                $novoValor[] = $value;
            }
        }

        $this->montarJs($dados, $substituir, $novoValor, $rand);
        $this->montarCss($dados['dependencias']['css'], $substituir, $novoValor, $rand);
        unset($dados['dependencias']);

        $this->enviarSaida($substituir, $novoValor, $rand, $dados, $this->pegarIndex());
    }

    private function iniciarAplicacaoReal($app){
        $dados = $this->identificarUrl(true, I::get('idPage'));

        $substituir = $novoValor = [];
        if(file_exists(DIR_BASE.'/'.SUBSTITUICAO_PASIVA)){
            $substituicaoPassivaText = str_replace(
                ['{PHP{ng-app}PHP}','{PHP{URLPROJETO}PHP}'],
                [Conteiner::get('ng-app'),PROTOCOLLO.'://'.URL_PROJETO],
                file_get_contents(DIR_BASE.'/'.SUBSTITUICAO_PASIVA));
            $substituicaoPassiva = json_decode($substituicaoPassivaText,true);
            foreach ($substituicaoPassiva as $key=>$value){
                $substituir[] = $key;
                $novoValor[] = $value;
            }
        }

        $this->montarJs($dados, $substituir, $novoValor, $app);
        $this->montarCss($dados['dependencias']['css'], $substituir, $novoValor, $app);
        unset($dados['dependencias']);

        $this->enviarSaida($substituir, $novoValor, $app, $dados, $this->pegarIndex());
    }

    private function montarReal(){
        $app = Conteiner::get('VERSAO_APP') ? Conteiner::get('ng-app').'-'.DIRETORIO_PROJETO.'-'.Conteiner::get('VERSAO_APP'): Conteiner::get('ng-app');
        if(file_exists( Conteiner::get('CAMINHO_JS').$app.'.js') && file_exists( Conteiner::get('CAMINHO_CSS').$app.'.css')){
            $dados = $this->identificarUrl(false, I::get('idPage'));
            $texto = $this->pegarIndex();

            if(file_exists(DIR_BASE.'/'.VARIAVEIS_AMBIENTE)){
                $dados['variaveisAmbiente'] = json_decode(file_get_contents(DIR_BASE.'/'.VARIAVEIS_AMBIENTE),true);
            }

            $substituir = $novoValor = [];
            if(file_exists(DIR_BASE.'/'.SUBSTITUICAO_PASIVA)){
                $substituicaoPassivaText = str_replace(
                ['{PHP{ng-app}PHP}','{PHP{URLPROJETO}PHP}', '{PHP{IDIOMA}PHP}'],
                [Conteiner::get('ng-app'),PROTOCOLLO.'://'.URL_PROJETO, IDIOMA],
                file_get_contents(DIR_BASE.'/'.SUBSTITUICAO_PASIVA));
            $substituicaoPassiva = json_decode($substituicaoPassivaText,true);
                foreach ($substituicaoPassiva as $key=>$value){
                    $substituir[] = $key;
                    $novoValor[] = $value;
                }
            }
            if(BASE_URL == 'BASE_URL'){
                define('BASE_URL','/');
            }
            if (APLICACAO_PRODUCAO == 1) {
                $substituir[] = '<!-- DGBLOBAL';
                $substituir[] = 'DGBLOBAL -->';
                $novoValor[] = '';
                $novoValor[] = '';
            }
            echo str_replace($substituir, $novoValor, str_replace(['<base href="/">', '{PHP{BASEURLPROJETO}PHP}','{PHP{Dominio}PHP}','{PHP{ng-app}PHP}', '{PHP{URLPROJETO}PHP} href="', '{PHP{URLPROJETO}PHP} src="', '/*{PHP{URLPROJETO}PHP}*/\'/u', '{PHP{URLPROJETO}PHP}', '{php{urlprojeto}php}="" href="', '{php{urlprojeto}php}="" src="', '{php{urlprojeto}php} href="', '{php{urlprojeto}php} src="', '{PHP{rand}PHP}', '{PHP{DGBLOBAL}PHP}', '{PHP{IDIOMA}PHP}', '{PHP{app}PHP}'],
						['<base href="'.BASE_URL.'">', BASE_URL, DOMINIO,Conteiner::get('ng-app'), 'href="'.PROTOCOLLO.'://'.URL_PROJETO, 'src="'.PROTOCOLLO.'://'.URL_PROJETO, '\''.PROTOCOLLO.'://'.URL_PROJETO.'u', PROTOCOLLO.'://'.URL_PROJETO, 'href="'.PROTOCOLLO.'://'.URL_PROJETO, 'src="'.PROTOCOLLO.'://'.URL_PROJETO, 'href="'.PROTOCOLLO.'://'.URL_PROJETO, 'src="'.PROTOCOLLO.'://'.URL_PROJETO, $app,'var DGlobal = '.json_encode($dados), IDIOMA,  Conteiner::get('app')],
						$texto));
        }else{
            $this->iniciarAplicacaoReal($app);
        }
    }

    public function montar(){
        if(APLICACAO_PRODUCAO == 1){
            $this->montarReal();
        }else{
            $this->montarDesenvolvimento();
        }
    }

}
