<?php
namespace Rubeus\FrontController;
use Rubeus\Servicos\Json\Json;
use Rubeus\Servicos\Entrada\I;
use Rubeus\ContenerDependencia\Conteiner;

abstract class ConfigurarAmbiente{
    static $configuracaoBase;

    public static function configurar($dir, $arq, $dir_extra = ''){
        I::conf();
        define('DIR_BASE', $dir);

        $config = Json::lerArq($dir.$dir_extra.$arq);

        foreach($config as $key => $value){
            if(!is_object($value)){
                switch($key){
                    case 'DIR_CONF': case 'ROTAS': case 'ARQ_BASE': case 'ARQ_ORM':
                    case 'DEPENDENCIA':  case 'ARQ_NAV': case 'DEFINE_APP':
                    case 'PROCESSO': case 'INTEGRACAOTOVS':
                        define($key, DIR_BASE.$dir_extra.$value);
                        break;
                    default :
                        define($key, $value);
                        break;
                }
            }
        }
        self::$configuracaoBase = $config;
        if($config->ARQ_CONFIG){
            $app = self::identificarApp(DIR_BASE.$dir_extra.$config->DEFINE_APP);
            self::registrarDefinicoes($config->ARQ_CONFIG, $app, $dir_extra);//usarContainer
        }
        if(isset($config->ARQ_CONFIG_DEFINE) && $config->ARQ_CONFIG_DEFINE){
            foreach($config->ARQ_CONFIG_DEFINE as $endConf){
                $conf = Json::lerArq($dir.$dir_extra.$endConf);

                foreach($conf as $key => $value){
                    define($key,$value);
                }
            }
        }
    }

    public function atualizarConfiguracao($app){
        self::registrarDefinicoes(self::$configuracaoBase->ARQ_CONFIG,$app);

        /*$controleNavegacao = Conteiner::get('ControleNavegacao');
        $controleNavegacao->iniciar(Conteiner::get('ARQ_NAV'), URL_PROJETO);*/
    }

    private static function getEndereco(){
        $endereco = strpos(I::server('REQUEST_URI'), '?') ?
                substr(I::server('REQUEST_URI'),0, strpos(I::server('REQUEST_URI'), '?')) : I::server('REQUEST_URI');

         if(!preg_match('/:[0-9]*/', URL_PROJETO,$resultado)){

            $array = explode('/', URL_PROJETO);
            $array[0]= "";
            $resultado = implode('/',$array);
        }else{

            $array = explode($resultado[0], URL_PROJETO);
            $resultado = $array[1];
        }
        if($resultado == '/'){
            $resultado = '';
            if(strpos($endereco,'/')==0){
                $endereco = substr($endereco,1,  strlen($endereco));
            }
        }
        return str_replace($resultado, '', $endereco);
    }

    private static function identificarApp($arq){

        $config = Json::lerArq($arq);
        $endereco = self::getEndereco();
        $end = explode("/",$endereco);
        $achou =0;

        foreach($config as $key => $value){
            if($value->url == "/".$end[0]){
                Conteiner::registrar('app', $key);
                Conteiner::registrar('ng-app', $value->ng_modulo);
                $achou =1;
            }else if($value->url == '/*'){
                $app = $key;
                $ngApp = $value->ng_modulo;
            }

        }
        if($achou == 0){
            Conteiner::registrar('app', $app);
            Conteiner::registrar('ng-app', $ngApp);
        }
        return Conteiner::get('app');
    }

    private static function registrarDefinicoes($config, $app, $dir_extra = ''){
        foreach($config as $key => $value){            
            if ($key == 'VERSAO') {
                $obj = Json::lerArq(DIR_BASE.$value);
            } else {
                $obj = Json::lerArq(DIR_BASE.$dir_extra.$value);
            }
            foreach($obj as $chave => $valor){
                if($chave == $app || $chave == 'root'){

                    foreach($valor as $c => $v){
                        switch($c){
                            case 'ARQ_NAV': case 'CAMINHO_JS': case 'CAMINHO_CSS': case 'INDEX':
                            case 'PocessoIntegracaoTotvs': case 'enderecoMapIntegracaoTotvs':
                                Conteiner::registrar($c , DIR_BASE.$v);
                                break;
                            case 'diretorioProcesso': case 'diretorioEventos':
                                $vc = array();
                                foreach($v as $vf){
                                    $vc[] = DIR_BASE.$vf;
                                }
                                Conteiner::registrar($c , $vc);
                                break;
                            case 'CONFIG_UPLOAD':
                                foreach($v as $ch=>$vf){
                                    $v->{$ch}->diretorio =  DIR_BASE.$v->{$ch}->diretorio;
                                    if(isset($v->{$ch}->url)){
                                        $v->{$ch}->url =  PROTOCOLLO."://".URL_PROJETO.$v->{$ch}->url;
                                    }
                                }

                                Conteiner::registrar($c , $v);
                                break;
                            default :
                                Conteiner::registrar($c , $v);
                                break;
                        }

                    }
                }
            }
        }
        $configBase = Conteiner::get('BASE_DADOS');
        foreach($configBase as $key => $value){
            define('base_'.$key, $value->base);
        }
    }
}
