<?php
namespace Rubeus\FrontController;
use Rubeus\Servicos\Entrada\Sessao;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\Bd\Persistencia;

abstract class Saida{
    static $dados = array();
    static $salvar = false;
    
    public static function ecoar($o='',$json=false) { 
        if(self::$salvar)
            self::$dados[] = $o;
        else{
            if(Conteiner::get('EstruturaRegistrarSessao')){
                Sessao::getEstrutura()->registrarSessao();
                $o['codsessrt'] = Sessao::getEstrutura()->getCodigoSessao(Sessao::get('idSess'));
                Persistencia::commit();
            }
        
            if($json){
                echo $o;
            }else{
                // header('Content-Type:application/json');
                echo json_encode($o, JSON_PARTIAL_OUTPUT_ON_ERROR);
            }
        }
    } 
    
}